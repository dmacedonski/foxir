# Lista zmian

### v2.1.0

* Dodanie opcji zamknięcia aplikacji poprzez wpisanie **quit**
* Aktualizacja README

### v2.0.0

* Zmiana języka programu na Python
