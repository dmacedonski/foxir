# Fox IR

### Czym jest Fox IR?

Jest to system umożliwiający kontrolę komputera z systemem Linux za pomocą dowolnego pilota np. od telewizora. System składa się z trzech elementów:

* kontrolera podłączonego za pomocą kabla USB do komputera,
* aplikacja zainstalowana na komputerze,
* dowolny pilot wykorzystujący diodę podczerwieni.

### Czego potrzebujesz

Układ jest oparty na płytce Arduino Leonardo wykorzystującej odbiornik IR do odczytywania sygnałów fal podczerwonych oraz diodę LED do sygnalizowania działania.

Kod programu, który należy wgrać na płytkę oraz schemat układu można znaleźć w zakładce *pobierz*.

Potrzebny jest także pilot, który umożliwi wysyłanie sygnałów do kontrolera.

### Wymagania

#### Układ do odbierania sygnałów

* Arduino Leonardo - może być także inna płytka z kontrolerem, ale program został przygotowywany i testowany w oparciu o ten model,
* dioda LED
* odbiornik IR - TSOP2236 - 36 kHz
* rezystory 100 Ω oraz 220 Ω

#### Wymagania sprzętowe

* system operacyjny Linux
* zainstalowany pakiet xautomation
* python 2.7 lub nowszy

### Uruchomienie

1. Zbuduj układ tak jak przedstawiono na schemacie,
2. Podłącz układ do komputera za pomocą kabla USB
3. Skompiluj i wgraj kod z katalogu FoxIR_Arduino na swoje Arduino,
4. Zainstaluj pakiet xautomation w swoim systemie **sudo aptitude install xautomation**,
5. Jeśli jeszcze nie masz zaistalowanego Pythona, zrób to,
6. Stwórz swoją mapę przycisków, przykład w katalogu *FoxIR_Linux/maps*, pierwsza wartość to kod przycisku pilota, a druga to akcja, aby określić kody przycisków możesz uruchomić program bez mapy przycisków w tedy po naciśnięciu przycisku na ekranie zostanie wyświetlony jego kod,
7. Uruchom program z katalogu *FoxIR_Linux* np. **python FoxIR.py maps/philips.map** lub **python FoxIR.py** aby uruchomić bez wczytania mapy przycisków
8. Aby zakończyć program wpisz **quit**

### Lista zmian

Szczegóły [tutaj](CHANGELOG.md)

### Licencja

System jest oparty na licencji MIT szczegóły [tutaj](LICENCE.md)

### Autor

Damian Macedoński <dmacedonski@gmail.com>
