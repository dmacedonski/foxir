#include <IRremote.h>

const int RECV_PIN = 11;
const int LED_PIN = 8;

IRrecv irrecv(RECV_PIN);
decode_results results;

void setup()
{
	Serial.begin(9600);
	irrecv.enableIRIn();
	pinMode(LED_PIN, OUTPUT);
}

void loop() {
	if (irrecv.decode(&results)) {
		Serial.println(results.value, HEX);
		irrecv.resume();
		digitalWrite(LED_PIN, HIGH);
		delay(500);
		digitalWrite(LED_PIN, LOW);
	}
	delay(500);
}
