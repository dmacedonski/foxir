#!/usr/bin/python
# -*- coding: utf-8 -*-

import serial

class Listener():

    __serialName = '/dev/ttyACM0'
    __serialPort = 19200
    __serialTimeout = 2
    __serialHandler = None

    @staticmethod
    def openSerial():
        Listener.__serialHandler = serial.Serial(Listener.__serialName, Listener.__serialPort, timeout = Listener.__serialTimeout)

    @staticmethod
    def closeSerial():
        Listener.__serialHandler.close()

    @staticmethod
    def readCode():
        return Listener.__serialHandler.readline().rstrip()
