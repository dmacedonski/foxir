#!/usr/bin/python
# -*- coding: utf-8 -*-

class Mapper():

    __keyMap = {}

    @staticmethod
    def loadMap(path):
        file = open(path, 'r')
        for line in file:
            pair = line.rstrip().split(' ', 1)
            if len(pair) > 1:
                Mapper.__keyMap[pair[0]] = pair[1]
        file.close()

    @staticmethod
    def getKey(key):
        if key in Mapper.__keyMap:
            return Mapper.__keyMap[key]
        else:
            raise Exception('Przycisk nie został przypisany.')
