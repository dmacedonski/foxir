#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import threading

from mapper import Mapper
from listener import Listener
from emiter import Emiter

class ThreadWorker (threading.Thread):

    __workStatus = False

    def __init__(self):
        threading.Thread.__init__(self)
        self.__workStatus = True

    def run(self):
        while True:
            if self.__workStatus == False:
                break
            code = None
            try:
                code = Listener.readCode()
                if code != '':
                    key = Mapper.getKey(code)
                    Emiter.sendKey(key)
            except Exception as e:
                print e.args[0], ' code: ', code
        Listener.closeSerial()

    def exit(self):
        self.__workStatus = False
        print 'Trwa zamykanie...'

try:
    if len(sys.argv) > 1:
        Mapper.loadMap(sys.argv[1])
    else:
        print 'Nie wskazano mapy przycisków.'
    Listener.openSerial()
    threadWorker = ThreadWorker()
    threadWorker.start()
    while True:
        userInput = raw_input('')
        if userInput == 'quit':
            threadWorker.exit()
            break
    threadWorker.join()
except Exception as e:
    print e.args[0]
