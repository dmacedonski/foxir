#!/usr/bin/python
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE

class Emiter():

    @staticmethod
    def sendKey(key):
        emiter = Popen(['xte'], stdin = PIPE)
        emiter.communicate(input = 'key ' + key + '\n')
